module gitlab.com/apokaliptis/replace-mod-error

go 1.20

require internal/utils v0.1.0

replace internal/utils => ./internal/utils
