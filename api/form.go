package form

import (
	"internal/utils"
	"net/http"
)

func Form(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Not a POST request.", http.StatusBadRequest)
		return
	}

	r.ParseForm()
	w.WriteHeader(http.StatusAccepted)
	utils.Success()
}
